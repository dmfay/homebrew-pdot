# homebrew-pdot

This is a Homebrew tap for installing [pdot](https://gitlab.com/dmfay/pdot).

```
brew tap dmfay/pdot git@gitlab.com:dmfay/homebrew-pdot.git
brew install dmfay/pdot/pdot
```
